//引入Vue核心库
import Vue from 'vue'
//引入Vuex
import Vuex from 'vuex'
// 引入模块 
import tab from './tab';
import dialog from './dialog';
//应用Vuex插件
Vue.use(Vuex)

//创建并暴露store
export default new Vuex.Store({
    //使用模块化引入
    modules:{
        tab,dialog
        
    }
    
    // actions,
    // mutations,
    // state
})


// //准备actions对象——响应组件中用户的动作
// const actions = {}
// //准备mutations对象——修改state中的数据
// const mutations = {}
// //准备state对象——保存具体的数据
// const state = {}
