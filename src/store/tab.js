export default {
    // //准备state——用于存储数据
    state:{
        isCollapse:false,
        token:"",
    },
    //准备mutations——用于操作数据（state）
    mutations:{
        collapseMenu(state){
            state.isCollapse= !state.isCollapse  // 取反
        }

    }

}

// //准备actions对象——响应组件中用户的动作
// const actions = {}
// //准备mutations对象——修改state中的数据
// const mutations = {}
// //准备state对象——保存具体的数据
// const state = {}
