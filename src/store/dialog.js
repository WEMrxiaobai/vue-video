export default {
    // //准备state——用于存储数据
    state:{
        dialogOpen:false,
        AddOrChange:false,
        method:'upt',
    }, 
    //准备mutations——用于操作数据（state）
    mutations:{
        dialogShowClose(state){
            // console.log('qufan',state.dialogOpen);
            state.dialogOpen= !state.dialogOpen  // 取反
        },
        dialogClose(state){
            state.dialogOpen=false;
        },
        dialogShow(state){
            state.dialogOpen=true; 
        },
        changeMethodUpt(state){
            state.method='upt'; 
        },
        changeMethodAdd(state){
            state.method='add'; 
        },
    }
}

