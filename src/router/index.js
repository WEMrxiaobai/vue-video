// 创建路由器 
import VueRouter from "vue-router"
// import store from '../store';	//引入store管理
// import service from "../utils/request";
// import About from '../views/About'
// import Home from '../views/Home'
// import Main from '../views/Main';
// import User from '../views/User';
// import Order from '../views/Order';
// import Message from '../views/Message';
// import Other1 from '../views/Other1';
// import Other2 from '../views/Other2';
// import Login from '../views/Login';
// import Player from '../views/player';
// import Video from '../views/Video';
// import Vindex from '../views/Video/Vindex';
// import TypeVideo from '../views/Video/TypeVideo';
// import DetailVideo from '../views/Video/DetailVideo';
// import Other from '../views/Video/Other';
const index = () => import('../views/index');
const Login = () => import('../views/Login');

const Main = () => import('../views/admin/Main');
const Order = () => import('../views/admin/Order');
const About = () => import('../views/admin/About');
const Home = () => import('../views/admin/Home');
// basic
const Category=() =>import('../views/admin/Basic/Category');
// video Mang 
const videoData= () =>import('../views/admin/Videomang/videoData');
const jxList= () =>import('../views/admin/Videomang/jxList');
const addVideo= () =>import('../views/admin/Videomang/addVideo');
const User = () => import('../views/admin/User');
const Message = () => import('../views/admin/Message');
const Other1 = () => import('../views/admin/Other1');
const Other2 = () => import('../views/admin/Other2');
const Player = () => import('../views/admin/player');

const Video = () => import('../views/Video');
const Vindex = () => import('../views/Video/Vindex');
const TypeVideo = () => import('../views/Video/TypeVideo');
const DetailVideo = () => import('../views/Video/DetailVideo');
const Other = () => import('../views/Video/Other');
const vplayer = () => import('../views/Video/Player');
// 创建一个路由
const router = new VueRouter({
    // mode: 'history',
    routes: [
        {
            path: '/',
            component: index,
            children: [
                {
                    // 网站主页
                    path: '/',
                    component: Video,
                    children: [
                        {
                            path: '',
                            component: Vindex,
                        },
                        {
                            path: 'index',
                            name: 'Vindex',
                            component: Vindex,
                        },
                        {
                            path: 'mv',
                            name: 'mv',
                            component: TypeVideo,
                            children: [
                                {
                                    path: '*',
                                    redirect: '/mv'
                                },
                            ]
                        },
                        {
                            path: 'tv',
                            name: 'tv',
                            component: TypeVideo,
                        },
                        {
                            path: 'ac',
                            name: 'ac',
                            component: TypeVideo,

                        },
                        {
                            path: 'doc',
                            name: 'doc',
                            component: TypeVideo,

                        },
                        {
                            path: 'va',
                            name: 'va',
                            component: TypeVideo,

                        },
                        {
                            path: 'detail',
                            name: 'DetailVideo',
                            component: DetailVideo,
                            // children:[
                            //     {
                            //         path:'*',
                            //         redirect:'/detail'
                            //     },
                            // ]
                        },
                        {
                            path: 'other',
                            name: 'Other',
                            component: Other,
                        },
                        {
                            path: 'vplayer',
                            name: 'vplayer',
                            component: vplayer,
                        },

                        // {
                        //     path:'*',
                        //     redirect:'/index'
                        // }
                    ]
                },
                {
                    path: 'admin',
                    component: Main,
                    children: [
                        {
                            path: '',
                            name: 'Home',
                            component: Home,
                        },
                        {
                            path: 'category',
                            name: 'category',
                            component: Category,
                        },
                        {
                            path:'videodata',
                            name:'videoData',
                            component:videoData,
                        },
                        {
                            path:'addvideo',
                            name:'addVideo',
                            component:addVideo,
                        },
  
                        {
                            path:'jxlist',
                            name:'jxList',
                            component:jxList,
                        },
                        {
                            path: 'user',
                            name: 'User',
                            component: User,
                        },
                        {
                            path: 'about',
                            name: 'About',
                            component: About,
                        },
                        {
                            path: 'order',
                            name: 'Order',
                            component: Order,
                        },
                        {
                            path: 'message',
                            name: 'Message',
                            component: Message,
                        },
                        {
                            path: 'player',
                            name: 'Player',
                            component: Player,
                        },
                        {
                            path: 'other1',
                            name: 'Other1',
                            component: Other1,
                        },
                        {
                            path: 'other2',
                            name: 'Other2',
                            component: Other2,
                        },
                        {
                            path: '*',
                            redirect: '/admin'
                        }
                    ]
                },
                {
                    path: '/login',
                    name: 'Login',
                    component: Login,

                },


            ]

        },


    ]
})
//  前置路由守卫
router.beforeEach((to, from, next) => {
    // console.log("路由");
    // console.log(to.path, from);
    var checkPath='';
    // console.log("store",store.getters.token);
    for (let i=0,len=router.options.routes[0].children[0].children.length;i<len;i++){
        if(to.path==='/'+router.options.routes[0].children[0].children[i].path){
            // console.log("to",'/'+router.options.routes[0].children[0].children[i].path);
            checkPath='/'+router.options.routes[0].children[0].children[i].path;
            break;
        }
    }
    // console.log("routerName:",router.options.routes[0].children[0].children[1].path);
    if (to.path === checkPath) {
        next();
    }
    if(to.path.slice(0,6)==="/admin" && localStorage.getItem("token")){
        next();
    }
    if(to.path.slice(0,6)==="/admin" && !localStorage.getItem("token")){
        next("/login")
    }
    if (to.path == "/login") {
        localStorage.removeItem("token");   
        next();
    }
    // var token = localStorage.getItem("token");
    // if (token == null && to.path == '/admin') {
    //     next("/login")
    // } 
    

})

// new Promise((resolve, reject) => {
//     service
//     .post("/admin/istoken",token )
//     .then((result) => {
//         if(result.err){
//             next("/login")
//             resolve({})
//         }else{
//             next()
//             reject({})
//         }

//     });
// }).catch(err => {
//    console.log("err 206",err);
// })
export default router